Title: gnome-desktop/gnome-mpv has been renamed to gnome-desktop/celluloid
Author: Pierre Lejeune <superheron@gmail.com>
Content-Type: text/plain
Posted: 2020-01-19
Revision: 2
News-Item-Format: 1.0
Display-If-Installed: gnome-desktop/gnome-mpv

Please install gnome-desktop/celluloid and *afterwards* uninstall gnome-desktop/gnome-mpv.

1. Take note of any packages depending on gnome-desktop/gnome-mpv:
cave resolve \!gnome-desktop/gnome-mpv

2. Install gnome-desktop-celluloid:
cave resolve gnome-desktop/celluloid -x

3. Re-install the packages from step 1.

4. Uninstall gnome-desktop/gnome-mpv
cave resolve \!gnome-desktop/gnome-mpv -x

Do it in *this* order or you'll potentially *break* your system.
