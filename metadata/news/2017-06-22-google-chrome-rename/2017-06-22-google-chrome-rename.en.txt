Title: net-www/google-chrome has been renamed to net-www/google-chrome-bin
Author: Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
Content-Type: text/plain
Posted: 06/22/2017
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-www/google-chrome

Please install net-www/google-chrome-bin and *afterwards* uninstall net-www/google-chrome.

1. Take note of any packages depending on net-www/google-chrome:
cave resolve \!net-www/google-chrome

2. Install net-www/google-chrome-bin:
cave resolve net-www/google-chrome-bin -x

3. Re-install the packages from step 1.

4. Uninstall net-www/google-chrome:
cave resolve \!net-www/google-chrome -x

Do it in *this* order or you'll potentially *break* your system.
