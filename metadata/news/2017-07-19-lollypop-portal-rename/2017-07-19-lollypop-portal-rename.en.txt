Title: media-sound/lollypop-portal has been moved to dev-python/lollypop-portal
Author: Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
Content-Type: text/plain
Posted: 06/22/2017
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media-sound/lollypop-portal

Please install dev-python/lollypop-portal and *afterwards* uninstall media-sound/lollypop-portal.

1. Take note of any packages depending on media-sound/lollypop-portal:
cave resolve \!media-sound/lollypop-portal

2. Install dev-python/lollypop-portal:
cave resolve dev-python/lollypop-portal -x

3. Re-install the packages from step 1.

4. Uninstall media-sound/lollypop-portal:
cave resolve \!media-sound/lollypop-portal -x

